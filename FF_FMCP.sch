EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "OpticalEVA FF-25Gx12 FMCP "
Date "2022-04-26"
Rev "1.0"
Comp "Karlsruhe Institute of Technology (KIT)"
Comment1 "IPE"
Comment2 "Luis Ardila"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7050 2900 8650 2900
Wire Wire Line
	7050 2800 8650 2800
Wire Wire Line
	7050 2700 8650 2700
Wire Wire Line
	7050 2600 8650 2600
Wire Wire Line
	7050 2400 8650 2400
Wire Wire Line
	7050 2300 8650 2300
$Comp
L power:GND #PWR?
U 1 1 61B0BA3E
P 6750 7100
F 0 "#PWR?" H 6750 6850 50  0001 C CNN
F 1 "GND" H 6755 6927 50  0000 C CNN
F 2 "" H 6750 7100 50  0001 C CNN
F 3 "" H 6750 7100 50  0001 C CNN
	1    6750 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61B0B80A
P 6450 7100
F 0 "#PWR?" H 6450 6850 50  0001 C CNN
F 1 "GND" H 6455 6927 50  0000 C CNN
F 2 "" H 6450 7100 50  0001 C CNN
F 3 "" H 6450 7100 50  0001 C CNN
	1    6450 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61B0B544
P 6150 7100
F 0 "#PWR?" H 6150 6850 50  0001 C CNN
F 1 "GND" H 6155 6927 50  0000 C CNN
F 2 "" H 6150 7100 50  0001 C CNN
F 3 "" H 6150 7100 50  0001 C CNN
	1    6150 7100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61B0B291
P 5850 7100
F 0 "#PWR?" H 5850 6850 50  0001 C CNN
F 1 "GND" H 5855 6927 50  0000 C CNN
F 2 "" H 5850 7100 50  0001 C CNN
F 3 "" H 5850 7100 50  0001 C CNN
	1    5850 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP104
U 1 1 61B09D12
P 6750 7100
F 0 "TP104" H 6808 7172 50  0001 L CNN
F 1 "TestPoint" H 6808 7127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 6950 7100 50  0001 C CNN
F 3 "~" H 6950 7100 50  0001 C CNN
F 4 " 262-2034" H 6750 7100 50  0001 C CNN "rs#"
	1    6750 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP103
U 1 1 61B09A85
P 6450 7100
F 0 "TP103" H 6508 7172 50  0001 L CNN
F 1 "TestPoint" H 6508 7127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 6650 7100 50  0001 C CNN
F 3 "~" H 6650 7100 50  0001 C CNN
F 4 " 262-2034" H 6450 7100 50  0001 C CNN "rs#"
	1    6450 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP102
U 1 1 61B0978A
P 6150 7100
F 0 "TP102" H 6208 7172 50  0001 L CNN
F 1 "TestPoint" H 6208 7127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 6350 7100 50  0001 C CNN
F 3 "~" H 6350 7100 50  0001 C CNN
F 4 " 262-2034" H 6150 7100 50  0001 C CNN "rs#"
	1    6150 7100
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP101
U 1 1 61B080B6
P 5850 7100
F 0 "TP101" H 5908 7172 50  0001 L CNN
F 1 "TestPoint" H 5908 7127 50  0001 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 6050 7100 50  0001 C CNN
F 3 "~" H 6050 7100 50  0001 C CNN
F 4 " 262-2034" H 5850 7100 50  0001 C CNN "rs#"
	1    5850 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 2200 7050 2200
Wire Wire Line
	8650 2100 7050 2100
Wire Wire Line
	7050 750  8650 750 
Wire Wire Line
	8650 3950 7050 3950
Wire Wire Line
	8650 3850 7050 3850
$Sheet
S 5000 650  2050 5400
U 619269F2
F0 "FMCP" 50
F1 "sch/fmcp.sch" 50
F2 "DP_M2C_N[0..23]" I R 7050 1600 50 
F3 "DP_M2C_P[0..23]" I R 7050 1500 50 
F4 "DP_C2M_N[0..23]" O R 7050 1300 50 
F5 "DP_C2M_P[0..23]" O R 7050 1200 50 
F6 "SDA" B R 7050 1800 50 
F7 "SCL" O R 7050 1900 50 
F8 "DPCLK_M2C_P0" I R 7050 3850 50 
F9 "DPCLK_M2C_N0" I R 7050 3950 50 
F10 "PG_M2C" I R 7050 750 50 
F11 "RX_RESET_L" O R 7050 2100 50 
F12 "RX_INT_L" I R 7050 2200 50 
F13 "RX_SELECT_L" O R 7050 2300 50 
F14 "RX_PRESENT_L" I R 7050 2400 50 
F15 "TX_INT_L" I R 7050 2700 50 
F16 "TX_SELECT_L" O R 7050 2800 50 
F17 "TX_PRESENT_L" I R 7050 2900 50 
F18 "TX_RESET_L" O R 7050 2600 50 
F19 "DPCLK_C2M_P3" O R 7050 5150 50 
F20 "DPCLK_C2M_N3" O R 7050 5250 50 
F21 "DPCLK_M2C_P1" I R 7050 4500 50 
F22 "DPCLK_M2C_N1" I R 7050 4600 50 
F23 "FPGA_CLK_C2M_P" O R 7050 5800 50 
F24 "FPGA_CLK_C2M_N" O R 7050 5900 50 
F25 "TX_Vadj_EN" O R 7050 3100 50 
F26 "TX_Vadj_SEL" O R 7050 3200 50 
F27 "3V3_SNS_P" B L 5000 5150 50 
F28 "3V3_SNS_N" B L 5000 5250 50 
F29 "12V_SNS_P" B L 5000 5450 50 
F30 "12V_SNS_N" B L 5000 5550 50 
F31 "1V8_SNS_P" B L 5000 4850 50 
F32 "1V8_SNS_N" B L 5000 4950 50 
F33 "TX_V3V3_EN" O R 7050 3300 50 
$EndSheet
$Sheet
S 8650 3750 1050 400 
U 63D78862
F0 "CLKS" 50
F1 "sch/clks.sch" 50
F2 "OSC_CLK_200M_P" O L 8650 3850 50 
F3 "OSC_CLK_200M_N" O L 8650 3950 50 
$EndSheet
$Sheet
S 8650 650  1050 200 
U 63AB152A
F0 "POWER" 50
F1 "sch/power.sch" 50
F2 "PG_3V8" O L 8650 750 50 
F3 "3V8_SNS_P" B R 9700 700 50 
F4 "3V8_SNS_N" B R 9700 800 50 
$EndSheet
Wire Wire Line
	8650 1900 7050 1900
Wire Wire Line
	8650 1800 7050 1800
Wire Bus Line
	7050 1600 8650 1600
Wire Bus Line
	7050 1500 8650 1500
Wire Bus Line
	7050 1300 8650 1300
Wire Bus Line
	7050 1200 8650 1200
Text Label 8100 1300 2    50   ~ 0
C2M_TX_N[0..23]
Text Label 8100 1200 2    50   ~ 0
C2M_TX_P[0..23]
Text Label 8100 1600 2    50   ~ 0
M2C_RX_N[0..23]
Text Label 8100 1500 2    50   ~ 0
M2C_RX_P[0..23]
Text Notes 6000 6800 0    98   ~ 20
TestPoints
Text Notes 4700 6800 0    98   ~ 20
Heatsink
$Comp
L Mechanical:Heatsink #HS101
U 1 1 5E7A3C9C
P 5000 7150
F 0 "#HS101" H 5142 7271 50  0001 L CNN
F 1 "Heatsink" H 5142 7180 50  0000 L CNN
F 2 "" H 5012 7150 50  0001 C CNN
F 3 "https://www.qats.com/DataSheet/ATS-X51310D-C1-R0" H 5012 7150 50  0001 C CNN
F 4 "ATS36780-ND" H 5000 7150 50  0001 C CNN "Digi-Key"
F 5 "ATS1935-ND" H 5000 7150 50  0001 C CNN "Digi-Key2"
	1    5000 7150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H101
U 1 1 5E423097
P 1000 7000
F 0 "H101" H 1100 7046 50  0000 L CNN
F 1 "M2.5_Pad" H 1100 6955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 1000 7000 50  0001 C CNN
F 3 "~" H 1000 7000 50  0001 C CNN
	1    1000 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID106
U 1 1 5E7F77C8
P 3750 7200
F 0 "FID106" H 3835 7246 50  0000 L CNN
F 1 "Fiducial" H 3835 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3750 7200 50  0001 C CNN
F 3 "~" H 3750 7200 50  0001 C CNN
	1    3750 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID104
U 1 1 5E7F77C2
P 3250 7200
F 0 "FID104" H 3335 7246 50  0000 L CNN
F 1 "Fiducial" H 3335 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3250 7200 50  0001 C CNN
F 3 "~" H 3250 7200 50  0001 C CNN
	1    3250 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID102
U 1 1 5E7F77BC
P 2750 7200
F 0 "FID102" H 2835 7246 50  0000 L CNN
F 1 "Fiducial" H 2835 7155 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2750 7200 50  0001 C CNN
F 3 "~" H 2750 7200 50  0001 C CNN
	1    2750 7200
	1    0    0    -1  
$EndComp
$Comp
L KIT_Graphic:LA_LOGO G2
U 1 1 5E7E8B14
P 10350 6800
F 0 "G2" H 10725 6800 50  0001 L CNN
F 1 "LA_LOGO" H 10350 6540 50  0001 C CNN
F 2 "KIT_Graphic:LA_LOGO_MASK_NoName" H 10370 6470 50  0001 C CNN
F 3 "" H 10350 6800 50  0001 C CNN
	1    10350 6800
	1    0    0    -1  
$EndComp
$Comp
L KIT_Graphic:KIT_LOGO G1
U 1 1 5E7E615F
P 9400 6850
F 0 "G1" H 9400 6586 60  0001 C CNN
F 1 "KIT_LOGO" H 9400 7114 60  0001 C CNN
F 2 "KIT_Graphic:KIT_LOGO_MASK_4.1x1.6mm" H 9400 6850 157 0001 C CNN
F 3 "" H 9400 6850 157 0001 C CNN
	1    9400 6850
	1    0    0    -1  
$EndComp
Text Notes 3050 6800 0    98   ~ 20
Fiducials
$Comp
L Mechanical:Fiducial FID105
U 1 1 5E7DC2D6
P 3750 7000
F 0 "FID105" H 3835 7046 50  0000 L CNN
F 1 "Fiducial" H 3835 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3750 7000 50  0001 C CNN
F 3 "~" H 3750 7000 50  0001 C CNN
	1    3750 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID103
U 1 1 5E7DBFC6
P 3250 7000
F 0 "FID103" H 3335 7046 50  0000 L CNN
F 1 "Fiducial" H 3335 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3250 7000 50  0001 C CNN
F 3 "~" H 3250 7000 50  0001 C CNN
	1    3250 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID101
U 1 1 5E7DB8DE
P 2750 7000
F 0 "FID101" H 2835 7046 50  0000 L CNN
F 1 "Fiducial" H 2835 6955 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2750 7000 50  0001 C CNN
F 3 "~" H 2750 7000 50  0001 C CNN
	1    2750 7000
	1    0    0    -1  
$EndComp
Text Notes 850  6800 0    98   ~ 20
Mounting Holes
$Comp
L Mechanical:MountingHole H104
U 1 1 5E424A41
P 1650 7200
F 0 "H104" H 1750 7246 50  0000 L CNN
F 1 "M2.5" H 1750 7155 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1650 7200 50  0001 C CNN
F 3 "~" H 1650 7200 50  0001 C CNN
	1    1650 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H103
U 1 1 5E424A3B
P 1650 7000
F 0 "H103" H 1750 7046 50  0000 L CNN
F 1 "M2.5" H 1750 6955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1650 7000 50  0001 C CNN
F 3 "~" H 1650 7000 50  0001 C CNN
	1    1650 7000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H102
U 1 1 5E42431F
P 1000 7200
F 0 "H102" H 1100 7246 50  0000 L CNN
F 1 "M2.5_Pad" H 1100 7155 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_Pad" H 1000 7200 50  0001 C CNN
F 3 "~" H 1000 7200 50  0001 C CNN
	1    1000 7200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J106
U 1 1 621069D0
P 9950 5800
F 0 "J106" H 10050 5729 50  0000 L CNN
F 1 "Conn_Coaxial" H 10050 5684 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 9950 5800 50  0001 C CNN
F 3 " ~" H 9950 5800 50  0001 C CNN
F 4 "Samtec" H 9950 5800 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 9950 5800 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 9950 5800 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 9950 5800 50  0001 C CNN "digikey#"
F 8 "158-7743" H 9950 5800 50  0001 C CNN "rs#"
	1    9950 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 621069DC
P 9950 6000
F 0 "#PWR?" H 9950 5750 50  0001 C CNN
F 1 "GND" H 9955 5827 50  0000 C CNN
F 2 "" H 9950 6000 50  0001 C CNN
F 3 "" H 9950 6000 50  0001 C CNN
	1    9950 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 621069E2
P 9550 6100
F 0 "#PWR?" H 9550 5850 50  0001 C CNN
F 1 "GND" H 9555 5927 50  0000 C CNN
F 2 "" H 9550 6100 50  0001 C CNN
F 3 "" H 9550 6100 50  0001 C CNN
	1    9550 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 4500 7050 4500
Wire Wire Line
	8650 4600 7050 4600
Wire Wire Line
	9050 5150 7050 5150
Wire Wire Line
	8650 5250 7050 5250
$Comp
L Connector:Conn_Coaxial J102
U 1 1 6223BCF8
P 8850 5250
F 0 "J102" H 8950 5179 50  0000 L CNN
F 1 "Conn_Coaxial" H 8950 5134 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 8850 5250 50  0001 C CNN
F 3 " ~" H 8850 5250 50  0001 C CNN
F 4 "Samtec" H 8850 5250 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 8850 5250 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 8850 5250 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 8850 5250 50  0001 C CNN "digikey#"
F 8 "158-7743" H 8850 5250 50  0001 C CNN "rs#"
	1    8850 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J105
U 1 1 6223C9FA
P 9550 5900
F 0 "J105" H 9650 5829 50  0000 L CNN
F 1 "Conn_Coaxial" H 9650 5784 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 9550 5900 50  0001 C CNN
F 3 " ~" H 9550 5900 50  0001 C CNN
F 4 "Samtec" H 9550 5900 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 9550 5900 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 9550 5900 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 9550 5900 50  0001 C CNN "digikey#"
F 8 "158-7743" H 9550 5900 50  0001 C CNN "rs#"
	1    9550 5900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J103
U 1 1 6223CD8C
P 9250 4500
F 0 "J103" H 9350 4429 50  0000 L CNN
F 1 "Conn_Coaxial" H 9350 4384 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 9250 4500 50  0001 C CNN
F 3 " ~" H 9250 4500 50  0001 C CNN
F 4 "Samtec" H 9250 4500 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 9250 4500 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 9250 4500 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 9250 4500 50  0001 C CNN "digikey#"
F 8 "158-7743" H 9250 4500 50  0001 C CNN "rs#"
	1    9250 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J101
U 1 1 6223D7A2
P 8850 4600
F 0 "J101" H 8950 4529 50  0000 L CNN
F 1 "Conn_Coaxial" H 8950 4484 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 8850 4600 50  0001 C CNN
F 3 " ~" H 8850 4600 50  0001 C CNN
F 4 "Samtec" H 8850 4600 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 8850 4600 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 8850 4600 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 8850 4600 50  0001 C CNN "digikey#"
F 8 "158-7743" H 8850 4600 50  0001 C CNN "rs#"
	1    8850 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J104
U 1 1 6223DB94
P 9250 5150
F 0 "J104" H 9350 5079 50  0000 L CNN
F 1 "Conn_Coaxial" H 9350 5034 50  0001 L CNN
F 2 "Connector_Coaxial:MMCX_Molex_73415-0961_Horizontal_1.6mm-PCB" H 9250 5150 50  0001 C CNN
F 3 " ~" H 9250 5150 50  0001 C CNN
F 4 "Samtec" H 9250 5150 50  0001 C CNN "manf"
F 5 "MMCX-J-P-H-ST-EM1" H 9250 5150 50  0001 C CNN "manf#"
F 6 " 200-MMCXJPHSTEM1 " H 9250 5150 50  0001 C CNN "mouser#"
F 7 "SAM8837-ND" H 9250 5150 50  0001 C CNN "digikey#"
F 8 "158-7743" H 9250 5150 50  0001 C CNN "rs#"
	1    9250 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 5800 8000 5800
Wire Wire Line
	7050 5900 7400 5900
$Comp
L Device:C C?
U 1 1 6227E31A
P 7550 5900
AR Path="/619BA206/6227E31A" Ref="C?"  Part="1" 
AR Path="/6227E31A" Ref="C101"  Part="1" 
F 0 "C101" V 7500 6000 50  0000 L CNN
F 1 "0.1uF" V 7500 5800 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 7588 5750 50  0001 C CNN
F 3 "~" H 7550 5900 50  0001 C CNN
F 4 "185-1596" H 7550 5900 50  0001 C CNN "rs#"
F 5 "0201, 25V, X5R, 10%" V 7390 5900 50  0001 C CNN "info"
F 6 "Luis" H 7550 5900 50  0001 C CNN "stock"
	1    7550 5900
	0    1    -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 6227E324
P 8150 5800
AR Path="/619BA206/6227E324" Ref="C?"  Part="1" 
AR Path="/6227E324" Ref="C102"  Part="1" 
F 0 "C102" V 8100 5900 50  0000 L CNN
F 1 "0.1uF" V 8100 5700 50  0000 R CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 8188 5650 50  0001 C CNN
F 3 "~" H 8150 5800 50  0001 C CNN
F 4 "185-1596" H 8150 5800 50  0001 C CNN "rs#"
F 5 "0201, 25V, X5R, 10%" V 7990 5800 50  0001 C CNN "info"
F 6 "Luis" H 8150 5800 50  0001 C CNN "stock"
	1    8150 5800
	0    1    -1   0   
$EndComp
Wire Wire Line
	9750 5800 8300 5800
Wire Wire Line
	9350 5900 7700 5900
Text Label 8500 5800 0    50   ~ 0
FPGA_CLK_C2M_C_P
Text Label 8500 5900 0    50   ~ 0
FPGA_CLK_C2M_C_N
Text Notes 550  1200 0    100  ~ 0
NOTES: \n1) PG_M2C signal is pulled up in VCU118\n2) FPGA_CLK needs capacitors in the mezzanine\n3) DPCLKs have capacitors in the VCU118
$Comp
L power:GND #PWR?
U 1 1 62364F3F
P 8850 5450
F 0 "#PWR?" H 8850 5200 50  0001 C CNN
F 1 "GND" H 8855 5277 50  0000 C CNN
F 2 "" H 8850 5450 50  0001 C CNN
F 3 "" H 8850 5450 50  0001 C CNN
	1    8850 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6236526A
P 9250 5350
F 0 "#PWR?" H 9250 5100 50  0001 C CNN
F 1 "GND" H 9255 5177 50  0000 C CNN
F 2 "" H 9250 5350 50  0001 C CNN
F 3 "" H 9250 5350 50  0001 C CNN
	1    9250 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6236558E
P 9250 4700
F 0 "#PWR?" H 9250 4450 50  0001 C CNN
F 1 "GND" H 9255 4527 50  0000 C CNN
F 2 "" H 9250 4700 50  0001 C CNN
F 3 "" H 9250 4700 50  0001 C CNN
	1    9250 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 623659C2
P 8850 4800
F 0 "#PWR?" H 8850 4550 50  0001 C CNN
F 1 "GND" H 8855 4627 50  0000 C CNN
F 2 "" H 8850 4800 50  0001 C CNN
F 3 "" H 8850 4800 50  0001 C CNN
	1    8850 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3200 8650 3200
Wire Wire Line
	7050 3100 8650 3100
$Sheet
S 8650 1100 1050 2350
U 619BA206
F0 "FF" 50
F1 "sch/ff.sch" 50
F2 "TX_P[0..23]" I L 8650 1200 50 
F3 "TX_N[0..23]" I L 8650 1300 50 
F4 "RX_P[0..23]" O L 8650 1500 50 
F5 "RX_N[0..23]" O L 8650 1600 50 
F6 "SDA" B L 8650 1800 50 
F7 "SCL" I L 8650 1900 50 
F8 "RX_RESET_L" I L 8650 2100 50 
F9 "RX_INT_L" O L 8650 2200 50 
F10 "RX_SELECT_L" I L 8650 2300 50 
F11 "RX_PRESENT_L" O L 8650 2400 50 
F12 "TX_RESET_L" I L 8650 2600 50 
F13 "TX_INT_L" O L 8650 2700 50 
F14 "TX_SELECT_L" I L 8650 2800 50 
F15 "TX_PRESENT_L" O L 8650 2900 50 
F16 "TX_Vadj_EN" I L 8650 3100 50 
F17 "TX_Vadj_SEL" I L 8650 3200 50 
F18 "TX_V3V3_EN" I L 8650 3300 50 
$EndSheet
Wire Wire Line
	8650 3300 7050 3300
$Sheet
S 2500 4700 1450 1350
U 62B45C3A
F0 "SENSE" 50
F1 "sch/sense.sch" 50
F2 "SDA" B R 3950 5750 50 
F3 "SCL" I R 3950 5850 50 
F4 "IN2_SNS_P" B R 3950 5150 50 
F5 "IN2_SNS_N" B R 3950 5250 50 
F6 "IN3_SNS_P" B R 3950 5450 50 
F7 "IN3_SNS_N" B R 3950 5550 50 
F8 "IN1_SNS_P" B R 3950 4850 50 
F9 "IN1_SNS_N" B R 3950 4950 50 
$EndSheet
Wire Wire Line
	3950 4850 5000 4850
Wire Wire Line
	3950 4950 5000 4950
Wire Wire Line
	3950 5150 5000 5150
Wire Wire Line
	3950 5250 5000 5250
Wire Wire Line
	3950 5750 4300 5750
Wire Wire Line
	3950 5850 4300 5850
Text Label 4300 5750 2    50   ~ 0
SDA
Text Label 4300 5850 2    50   ~ 0
SCL
Text Label 7850 1800 2    50   ~ 0
SDA
Text Label 7850 1900 2    50   ~ 0
SCL
Wire Wire Line
	3950 5450 4500 5450
Wire Wire Line
	3950 5550 4500 5550
Wire Wire Line
	9700 700  10250 700 
Text Label 10250 700  2    50   ~ 0
3V8_SNS_P
Wire Wire Line
	9700 800  10250 800 
Text Label 10250 800  2    50   ~ 0
3V8_SNS_N
Text Label 4500 5450 2    50   ~ 0
3V8_SNS_P
Text Label 4500 5550 2    50   ~ 0
3V8_SNS_N
$Comp
L KIT_Graphic:Lilith G101
U 1 1 6268E3F1
P 10950 6850
F 0 "G101" H 10950 6599 60  0001 C CNN
F 1 "Lilith" H 10950 7101 60  0001 C CNN
F 2 "KIT_Graphic:Lilith" H 10950 6850 100 0001 C CNN
F 3 "" H 10950 6850 100 0001 C CNN
	1    10950 6850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
